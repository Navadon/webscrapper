#!/usr/local/bin/python
from bs4 import BeautifulSoup
import requests


def get_page(url):
    req = requests.get(url)
    data = req.text
    spobj = BeautifulSoup(data, "lxml")
    return spobj


def main():
    headers = []

    # loop over each page
    for page in range(0, 4):
        # url for each page
        url = "https://www.heise.de/thema/https?seite=" + str(page)

        # get titles
        page_headers = get_page(url).find("div", {"class": "keywordliste"})
        page_headers = page_headers.findAll("header")
        page_headers = [str(header).replace('<header>', '').replace('</header>', '').replace('\n', '').strip() for header in page_headers]
        headers += page_headers[1:]

    # get top 3 used words
    header_string = ' '.join(headers)
    words = list(set(header_string.split(' ')))
    word_counts = [(word, header_string.count(word)) for word in words if (len(word) > 1)]

    # print out result
    print(sorted(word_counts, key=lambda tup: tup[1])[-3:])

if __name__ == '__main__':
    main()
